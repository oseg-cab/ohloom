/*File Info--------------------------------------------------------------------
File Name: RatchetPawl.scad
Project Name: OpenHardware LOOM - OHLOOM
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the OHLOOM Project. Original design by Oliver Slueter, who made all wooden parts without a CNC Router. https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM
Usage: 
./OHLoom_Documentation/Assembly_Guide/AssemblyGuide.md
./OHLoom_Documentation/User_Guide/OHLOOM_UserGuide.md
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------

length=130;
thickness=20;
hole=5;

$fn=80;

module triangle_prism(b,d,h,ht,sc)
{
    linear_extrude(height=ht,scale=sc)
    polygon(points=[[0,0],[b,0],[-thickness/2,h]]);
}

difference()
{
    hull()
    {
        translate([-length/2+thickness/2,thickness/2,0])
        cylinder(h=thickness,d=thickness);
        translate([length/2-thickness/2-10,0,0])
        triangle_prism(thickness,0,thickness,thickness,1);
    }
    translate([-length/2+thickness/2,thickness/2,0])
    cylinder(h=thickness,d=hole);
    
}